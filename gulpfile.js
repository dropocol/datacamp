var gulp = require('gulp'),
    nodemon = require('gulp-nodemon');

// gulp.task('default', function(){
//     nodemon({
//         script: 'app.js',
//         ext: 'js',
//         ignore: ['./node_modules/**' , './courses/**' , './public/**'],
//         env: {
//             'NODE_ENV': 'development' , 
//             'FILE_NAME':'dev' 
//         }
//     })
//     .on('restart', function(){
//         console.log('Restarting');
//     });
// });



gulp.task('default', function () {
    nodemon({
        script: 'app.js',
        ext: 'js',
        ignore: ['./node_modules/**', './courses/**', './public/**'],
        env: {
            'COURSE_FOLDER': './courses',
            'DB_NAME': 'datacamp',
            'FILE_NAME': 'dev'
        }
    })
        .on('restart', function () {
            console.log('Restarting');
        });
});



gulp.task('dev', function () {
    nodemon({
        script: 'app.js',
        ext: 'js',
        ignore: ['./node_modules/**', './dev_courses/**', './courses/**', './public/**'],
        env: {
            'COURSE_FOLDER': './dev_courses',
            'DB_NAME': 'dev_datacamp',
            'FILE_NAME': 'dev'
        }
    })
        .on('restart', function () {
            console.log('Restarting');
        });
});
