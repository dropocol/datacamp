POSTMAN Link
https://www.getpostman.com/collections/020e34e87cd0e466edfa

Import DB & Export DB 
Change the address accordingly

mongodump --db datacamp -o database
mongorestore --db datacamp --dir database/datacamp


mongodump --db dev_datacamp -o database
mongorestore --db dev_datacamp --dir database/datacamp


nvm use

Steps
1 - npm install.
2 - Install gulp and gulp-nodemon.
3 - Use Robo3T for GUI based app of mongodb.
4 - Use postman to perform operations. The file link is given at the top.
5 - Import the database from the 'database' folder using the 'mongorestore' command given above.
6 - use 'gulp' to run it for normal use while 'gulp dev' is for develpment.
7 - Make sure you have 2 folders named 'courses' and 'dev_courses' in your app's root folder.



