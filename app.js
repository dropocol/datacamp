'use strict';

// require('dotenv').config({path: "environment/.env." + process.env.FILE_NAME});
var phantom         = require('phantom');
var express             = require('express'),
    app                 = express(),
    server              = require('http').Server(app),
    mongoose            = require('mongoose'),
    port                = process.env.port || 3001,
    bodyParser          = require('body-parser'),
    path                = require('path'),
    ejs                 = require('ejs')


mongoose.Promise        = global.Promise;

var options = {};
options.useMongoClient =  true
options.autoIndex      =  true
mongoose.set('debug', false); 
mongoose.connect('mongodb://localhost:27017/' + process.env.DB_NAME ,options);
global.models  = require('./api/model/');


// Handle the body of the request e.g req.body
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(express.static('public'));

// set the view engine to ejs
app.set('view engine', 'ejs');
global.ejs = ejs

// User route object
var datacampRoute      = require('./api/route/datacampRoute')();
var websiteRoute       = require('./api/route/websiteRoute')();


// User route to perform CURD ops.
app.use('/',websiteRoute);
// app.use('/',function(req,res){
//     console.log(req.originalUrl)
//     console.log(req.params)
//     console.log(req.query)
// });
app.use('/data/',datacampRoute);


// Gives hello word but needs verification performed on top.
app.get('/' ,function (req, res) {
    res.send('Hello World! ' + process.env.APP_NAME);
});


server.listen(port, function () {
    console.log('DataCamp - DL is Running');   
    console.log(process.env.COURSE_FOLDER)
});


module.exports = app