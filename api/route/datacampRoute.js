'use strict';

var express = require('express');

var routes = function () {

    var datacampRoute = express.Router();
    var searchController = require('../controller/searchController')();
    var downloadController = require('../controller/downloadController')();
    
    
    datacampRoute.route('/track/all_tracks')
    .get(searchController.getAllTracks);

    datacampRoute.route('/track/:search_track')
    .get(searchController.getTrackBasicInfo);
    
    datacampRoute.route('/track/delete/:search_track')
    .get(searchController.deleteTrack);

    //---------------------------------------------------

    datacampRoute.route('/all_courses')
    .get(searchController.getAllCourses);

    datacampRoute.route('/course/download_all')
    .get(downloadController.downloadAllCourses);

    datacampRoute.route('/course/:search_course')
    .get(searchController.getCourse);

    datacampRoute.route('/course/delete/:search_course')
    .get(searchController.deleteCourse);


    //---------------------------------------------------

    datacampRoute.route('/chapter/:chapter_id')
    .get(searchController.getExercisesLinksUsingChapterId);
    

    //---------------------------------------------------

    datacampRoute.route('/track/download/:search_track')
    .get(downloadController.downloadTrack);

    datacampRoute.route('/course/download/:search_course')
    .get(downloadController.downloadSingleCourse);

    
    //---------------------------------------------------


    datacampRoute.route('/remove')
    .get(searchController.removeDatabase);



    return datacampRoute;
};

module.exports = routes;