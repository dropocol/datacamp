'use strict';

var express = require('express');

var routes = function () {

    var websiteRoute = express.Router();
    var websiteController = require('../controller/websiteController')();

    websiteRoute.route('/')
    .get(websiteController.returnTracks);

    websiteRoute.route('/skill_tracks')
    .get(websiteController.returnTracks);

    websiteRoute.route('/career_tracks')
    .get(websiteController.returnTracks);

    websiteRoute.route('/all_courses')
    .get(websiteController.returnCourses);


    //---------------------------------------------------


    websiteRoute.route('/tracks/:track_link')
    .get(websiteController.returnTrackCourses);

    websiteRoute.route('/courses/:course_link')
    .get(websiteController.returnCourseExercises);


    //---------------------------------------------------

    websiteRoute.route('/courses/:course_link/images/:image_name')
    .get(websiteController.returnCourseImage);

    websiteRoute.route('/courses/:course_link/fonts/:font_name')
    .get(websiteController.returnCourseImage);

    websiteRoute.route('/courses/:course_link/:chapter_name')
    .get(websiteController.returnExercisesAddress);


    //---------------------------------------------------


    websiteRoute.route('/courses/:course_name/:chapter_name/video/:video_name')
    .get(websiteController.returnCourseVideo);

    websiteRoute.route('/courses/:course_name/:chapter_name/mp3/:video_name')
    .get(websiteController.returnCourseAudio);


    

    return websiteRoute;
};

module.exports = routes;