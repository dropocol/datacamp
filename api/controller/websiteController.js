var     path            = require("path");
var     async           = require('async');
const   cheerio         = require('cheerio')
const   fs              = require('fs');


var     trackController = require('./trackController');
var     courseController = require('./courseController');
var     chapterController = require('./chapterController');
var     exerciseController = require('./exerciseController');


var websiteController = function (){

    var returnTracks = async function(req , res , next){

        // if(req.params.search_track == undefined){
        //     return res.status(200).json({ 
        //         success: false,
        //         message: 'Please mention a track'
        //      });
        // }

        const filename = path.join(__dirname, '../../public/views', 'all-tracks.ejs')
        var data = await trackController.getAllTrackLinksInDB(req.originalUrl);
        res.render(filename, {data : data});

        // const filename = path.join(__dirname, '../../public', 'index.html')
        // res.sendFile(filename)

    }



    var returnCourses = async function(req , res , next){

        // if(req.params.search_track == undefined){
        //     return res.status(200).json({ 
        //         success: false,
        //         message: 'Please mention a track'
        //      });
        // }

        const filename = path.join(__dirname, '../../public/views', 'all-courses.ejs')
        var data = await courseController.getAllCourseLinksInDB();
        res.render(filename, {data : data});
    }


    var returnTrackCourses = async function(req , res , next){

        const filename = path.join(__dirname, '../../public/views', 'track-courses.ejs')
        var data = await trackController.getTrackCoursesInDB(req.params.track_link);
        res.render(filename, {data : data});


    }



    var returnCourseExercises = async function(req , res , next){

        const filename = path.join(__dirname, '../../public/views', 'course-chapters.ejs')
        var data = await courseController.getCourseChaptersInDB(req.params.course_link);
        res.render(filename, {data : data});

    }


    var returnExercisesAddress = async function(req , res , next){
        
        console.log(req.originalUrl);
        var exerciseLink    = req.originalUrl
        var result          = await exerciseController.returnExercisesAddress(exerciseLink);
        const filename      = path.join(__dirname, result.address, result.name);
        var   html          = fs.readFileSync(filename);

        if(result.type == 'webpage'){
    
            var $ = cheerio.load(html);
            $('link').each(function(i, elem) {
                if($(this).attr('rel') == 'stylesheet'){
                    $(this).attr('href', '/css/style.css');     
                }
            });
        
    
    
            $('script').each(function(i, elem) {
                var src = $(this).attr('src')
                if(src == undefined){
                    return
                }
    
                if(src.indexOf('public') == 1){
                    $(this).attr('src', '/js/client.js');          
                }
    
            });
            
    
            return res.send($.html());
        
        }

        else if(result.type == 'projector' || result.type == 'video' || result.type == 'audio'){

            var $ = cheerio.load(html);
            
            $('link').each(function(i, elem) {
                
                var src = $(this).attr('href');

                if(src == undefined){
                    return
                }

                if(src.indexOf('katex.min.css') != -1){
                    $(this).attr('href', '/css/katex.min.css');   
                    $(this).removeAttr('integrity').html();  
                }

                if(src.indexOf('video-js.css') != -1){
                    $(this).attr('href', '/css/video-js.css');     
                }

                if(src.indexOf('reveal.css') != -1){
                    $(this).attr('href', '/css/reveal.css');     
                }

                if(src.indexOf('white.css') != -1){
                    $(this).attr('href', '/css/white.css');     
                }

            });
        
    
    
            $('script').each(function(i, elem) {
                var src = $(this).attr('src')

                if(src == undefined){
                    return
                }
    
                if(src.indexOf('bb4a0452c1.js') != -1){
                    $(this).attr('src', '/js/bb4a0452c1.js');          
                }

                if(src.indexOf('katex.min.js') != -1){
                    $(this).attr('src', '/js/katex.min.js');   
                     
                }

                if(src.indexOf('auto-render.min.js') != -1){
                    $(this).attr('src', '/js/auto-render.min.js');          
                }

                if(src.indexOf('/bundle.js') != -1){
                    $(this).attr('src', '/js/bundle.js');          
                }

    
            });



            var videoJson  = {}
            var slidesJson = {}
            // console.log(result)
            $('input').each(function(i, elem) {

                if($(this).attr('id') == 'slideDeckData' && $(this).attr('value') != null){
                   
                    var slidesJson = JSON.parse($(this).attr('value'));
                    // console.log(slidesJson)
                    if(slidesJson == null){
                        return
                    }
                    
                    
                    if(slidesJson.plain_video_mp4_link !== null){
                        if(result.projectorData !== undefined){
                            if(result.projectorData.type == 'video'){
                                // console.log('Changed Value - 1');
                                slidesJson.plain_video_hls_link = null;
                                slidesJson.plain_video_mp4_link = result.projectorData.videoLink;
                            }
                        }
                    }

                    if(slidesJson.video_mp4_link !== null){
                        if(result.videoData !== undefined){
                            if(result.type == 'video'){
                                // console.log('Changed Value - 2');
                                slidesJson.video_hls_link = null;
                                slidesJson.video_mp4_link = result.videoData.videoLink;
                            }
                        }
                    }


                    if(slidesJson.audio_link !== null){
                        if(result.projectorData !== undefined){
                            if(result.projectorData.type == 'audio'){
                                // console.log('Changed Value - 3');
                                slidesJson.audio_link = result.projectorData.audioLink;
                            }
                        }
                    }

                    $(this).attr('value' , JSON.stringify(slidesJson));

                }
                else if($(this).attr('id') == 'videoData' && $(this).attr('value') != null){
                       
                    var videoJson = JSON.parse($(this).attr('value'));
                    // console.log(videoJson)
                    if(videoJson == null){
                        return
                    }
                    
                    if(videoJson.video_mp4_link !== null){
                        if(result.videoData !== undefined){
                            if(result.type == 'video'){
                                // console.log('Changed Value - 4');
                                videoJson.video_hls_link = null;
                                videoJson.video_mp4_link = result.videoData.videoLink;
                            }
                        }
                    }


                    if(videoJson.audio_link !== null){
                        if(result.projectorData !== undefined){
                            if(result.type == 'audio' || result.projectorData.type == 'audio'){
                                // console.log('Changed Value - 5');
                                videoJson.video_hls_link = null;
                                videoJson.audio_link = result.projectorData.audioLink;
                            }
                        }
                    }


                    $(this).attr('value' , JSON.stringify(videoJson));
                }
            
            });

            return res.send($.html());
        
        } 



    }




    var returnCourseImage = async function(req , res , next){

        var search = req.params.course_link
        var course = await courseController.getFullCourseDataByLink(search);

        var courseDirectory = course.name;
        var absoluteAddress = '';
        if(req.params.image_name !== undefined){
            absoluteAddress = path.resolve(process.env.COURSE_FOLDER) + '/'+ courseDirectory + '/images/'

        }else{
            absoluteAddress = path.resolve(process.env.COURSE_FOLDER) + '/'+ courseDirectory + '/fonts/'
        }
        
        var filename = '';
        if(req.params.image_name !== undefined){
            filename = absoluteAddress + req.params.image_name

        }else{
            filename = absoluteAddress + req.params.font_name
        }
         
        var img = fs.readFileSync(filename);

        if(req.params.image_name !== undefined){
            if(req.params.image_name.indexOf('png') == 0 || req.params.image_name.indexOf('PNG') == 0){
                res.writeHead(200, {'Content-Type': 'image/png' });
            }
            
            if(req.params.image_name.indexOf('jpeg') == 0 || req.params.image_name.indexOf('JPEG') == 0){
                res.writeHead(200, {'Content-Type': 'image/jpeg' });
            }
    
        }else{
            if(req.params.font_name.indexOf('svg')){
                res.writeHead(200, {'Content-Type': 'image/svg+xml' });
            }
        }

        res.end(img, 'binary');

    }



    var returnCourseAudio = async function(req , res , next){

        // var link = ''
        // if(req.params.video_name.indexOf('%20') != -1){
        //     link = '/courses/' + req.params.course_name + '/mp3/' + req.params.video_name.replace('%20' , ' ');
        // }else{
        //     link = '/courses/' + req.params.course_name + '/mp3/' + req.params.video_name;
        // }


        var link = ''
        if(req.originalUrl.indexOf('%20') != -1){
            link = req.originalUrl.replace(new RegExp('%20', 'g'), ' ');
        }else{
            link = req.originalUrl;
        }

        var result = await exerciseController.returnExercisesMediaAddress(link);
        var address  = path.join(__dirname, result.address, result.name);
        console.log(address)

        var filestream = fs.createReadStream(address);
        var range = req.headers.range.replace("bytes=", "").split('-');
        
        filestream.on('open', function() {
          var stats = fs.statSync(address);
          var fileSizeInBytes = stats["size"];
        
          // If the start or end of the range is empty, replace with 0 or filesize respectively
          var bytes_start = range[0] ? parseInt(range[0], 10) : 0;
          var bytes_end = range[1] ? parseInt(range[1], 10) : fileSizeInBytes;
        
          var chunk_size = bytes_end - bytes_start;
        
          if (chunk_size == fileSizeInBytes) {
            // Serve the whole file as before
            res.writeHead(200, {
              "Accept-Ranges": "bytes",
              'Content-Type': 'audio/mp3',
              'Content-Length': fileSizeInBytes});
            filestream.pipe(res);
          } else {
            // HTTP/1.1 206 is the partial content response code
            res.writeHead(206, {
              "Content-Range": "bytes " + bytes_start + "-" + bytes_end + "/" + fileSizeInBytes,
              "Accept-Ranges": "bytes",
              'Content-Type': 'audio/mp3',
              'Content-Length': fileSizeInBytes
            });
            filestream.pipe(res);
          }
        });
    }




    var returnCourseVideo = async function(req , res , next){


        if(req.params.video_name == undefined){
            return;
        }

        var link = ''
        if(req.originalUrl.indexOf('%20') != -1){
            link = req.originalUrl.replace(new RegExp('%20', 'g'), ' ');
        }else{
            link = req.originalUrl;
        }
       
        console.log(link)
        var result = await exerciseController.returnExercisesMediaAddress(link);
        var address  = path.join(__dirname, result.address, result.name);
        console.log(address)

        var filestream = fs.createReadStream(address);
        var range = req.headers.range.replace("bytes=", "").split('-');
        
        filestream.on('open', function() {
          var stats = fs.statSync(address);
          var fileSizeInBytes = stats["size"];
        
          // If the start or end of the range is empty, replace with 0 or filesize respectively
          var bytes_start = range[0] ? parseInt(range[0], 10) : 0;
          var bytes_end = range[1] ? parseInt(range[1], 10) : fileSizeInBytes;
        
          var chunk_size = bytes_end - bytes_start;
        
          if (chunk_size == fileSizeInBytes) {
            // Serve the whole file as before
            res.writeHead(200, {
              "Accept-Ranges": "bytes",
              'Content-Type': 'video/mpeg',
              'Content-Length': fileSizeInBytes});
            filestream.pipe(res);
          } else {
            // HTTP/1.1 206 is the partial content response code
            res.writeHead(206, {
              "Content-Range": "bytes " + bytes_start + "-" + bytes_end + "/" + fileSizeInBytes,
              "Accept-Ranges": "bytes",
              'Content-Type': 'video/mpeg',
              'Content-Length': fileSizeInBytes
            });
            filestream.pipe(res);
          }
        });
    }



    return {
        returnTracks            :           returnTracks,
        returnCourses           :           returnCourses,
        returnTrackCourses      :           returnTrackCourses,
        returnCourseExercises   :           returnCourseExercises,
        returnExercisesAddress  :           returnExercisesAddress,
        returnCourseImage       :           returnCourseImage,
        returnCourseAudio       :           returnCourseAudio,
        returnCourseVideo       :           returnCourseVideo
    };

};


module.exports = websiteController;