var     phantom             = require('phantom');
const   cheerio             = require('cheerio')
var     rp                  = require('request-promise');

var     trackController         = require('./trackController');
var     courseController        = require('./courseController');
var     chapterController       = require('./chapterController');
var     exerciseController      = require('./exerciseController');
var     fileController          = require('./fileController');
var     downloadFile            = require('./download');




function replaceIllegalCharacters(str) {
    var string = str
    string =  string.replace(new RegExp('/ ', 'g'), '-');
    string =  string.replace(new RegExp('/', 'g'), '-');
    string =  string.replace(new RegExp('&amp;', 'g'), 'and');
    string =  string.replace(new RegExp(': ', 'g'), '-');
    string =  string.replace(new RegExp(':', 'g'), '-');
    return string;
}

module.exports = {



    // extractDownloadLinkForVideo : async function(link){

    //     var promise =  new Promise(function(resolve, reject){ 
    //         rp(link)
    //         .then(function (htmlString) {

    //             var $    = cheerio.load(htmlString);
    //             var url  = '';
    //             var json = {}
    //             var data = {};
    //             $('input').each(function(i, elem) {
                    
    //                 json = JSON.parse($(this).attr('value'));
                    
    //                 if(json != null){
    //                     if(json.video_mp4_link !== undefined && json.video_mp4_link !== null){
    //                         url = json.video_mp4_link;
    //                         if(url.indexOf('https://') == -1){
    //                             url = url.replace("//", "https://");
    //                         }
    
    //                         data.url = url    
    //                         resolve(data);                        
    //                         return false
    //                     }
    
    //                     else if(json.plain_video_mp4_link !== undefined && json.plain_video_mp4_link !== null){
    //                         url = json.plain_video_mp4_link;
                            
    //                         if(url.indexOf('https://') == -1){
    //                             url   = url.replace("//", "https://");
    //                         }
    
    //                         data.url = url
    //                         resolve(data);                        
    //                         return false
    
    //                     }else{
    
    //                         if(json.audio_link !== undefined && json.audio_link !== null){
    //                             data.audioLink      = json.audio_link;
    //                             data.projector      = true;
    //                             data.projectorLink  = link;
    //                         }
    
    //                         resolve(data);                        
    //                         return false
    //                     }
    //                 }
                    
    //             });

    //         })
    //             .catch(function (err) {
    //             console.log(err)
    //         });
        
    //     });

    //     return promise;

    // },







    extractDownloadLinkForVideo : async function(link){

        var promise =  new Promise(function(resolve, reject){ 
            rp(link)
            .then(function (htmlString) {

                var $    = cheerio.load(htmlString);
                var url  = '';
                var videoJson  = {}
                var slidesJson = {}
                var data = {};


                $('input').each(function(i, elem) {
                    
                    if($(this).attr('id') == 'slideDeckData' && $(this).attr('value') != null){
                        slidesJson = JSON.parse($(this).attr('value'));
                    //    console.log(slidesJson);
                        if(slidesJson != null){
                            
                            var projectorData            = {};
                            var videoData                = {};

                            if(slidesJson.plain_video_mp4_link !== undefined && slidesJson.plain_video_mp4_link !== null){
                                
                                url = slidesJson.plain_video_mp4_link;
                                if(url.indexOf('https://') == -1){
                                    url = url.replace("//", "https://");
                                }
                                
                                projectorData.videoLink          = url;
                                projectorData.projectorLink      = link;
                                projectorData.type               = 'video';
                                data.projectorData               = projectorData;
                                
        
                            }else if(slidesJson.audio_link !== undefined && slidesJson.audio_link !== null){

                                url = slidesJson.audio_link;
                                if(url.indexOf('https://') == -1){
                                    url = url.replace("//", "https://");
                                }

                                projectorData.audioLink         = url;
                                projectorData.projectorLink     = link;
                                projectorData.type              = 'audio';
                                data.projectorData              = projectorData;
                            }

                            data.projector  = true;
                            return false

                        }
                    }
                });



                $('input').each(function(i, elem) {
                    
                    if($(this).attr('id') == 'videoData' && $(this).attr('value') != null){
                        videoJson = JSON.parse($(this).attr('value'));
                        // console.log(videoJson)
                        if(videoJson != null){

                            if(videoJson.video_mp4_link !== undefined && videoJson.video_mp4_link !== null){

                                url = videoJson.video_mp4_link;
                                if(url.indexOf('https://') == -1){
                                    url = url.replace("//", "https://");
                                }

                                var videoData                = {};
                                videoData.videoLink          = url  
                                videoData.projectorLink      = link
                                data.videoData               = videoData;

                            }else if(videoJson.audio_link !== undefined && videoJson.audio_link !== null){

                                url = videoJson.audio_link;
                                if(url.indexOf('https://') == -1){
                                    url = url.replace("//", "https://");
                                }

                                if(data.projectorData == undefined){
                                    var projectorData               = {};
                                    projectorData.audioLink         = url;
                                    projectorData.projectorLink     = link;
                                    projectorData.type              = 'audio';
                                    data.projectorData              = projectorData;
                                }

                            }

                            return false
                        }
                    }
                });

                return resolve(data);                        

            })
                .catch(function (err) {
                console.log(err)
            });
        
        });

        return promise;

    },




    updateExerciseVideoLink : async function(exerciseId , data){
        var exercise = await models.exercise.findById(exerciseId).exec()
        console.log('Updating exercise video link');

        if(data.projectorData !== undefined && data.videoData !== undefined){
            exercise.projectorData = data.projectorData;
            exercise.videoData     = data.videoData;
            exercise.type          = 'projector';
       
        }
        else if(data.projectorData !== undefined && data.videoData == undefined){
            exercise.projectorData = data.projectorData;
            exercise.type          = 'projector';

        }
        else if(data.projectorData == undefined && data.videoData !== undefined){
            exercise.videoData     = data.videoData;
            exercise.type          = 'video';
        }


        exercise.complete          = true;
        await exercise.save();
        return

    },




    


    searchExerciseOnlineByLink : async function(link){


        var promise =  new Promise(function(resolve, reject){ 

            console.log('Analyzing Exercise page : ' + link);
            rp(link)
            .then(function (htmlString) {

                var $ = cheerio.load(htmlString);
                var data = {}

                //console.log($('script[charset=UTF-8]').eq(1).html());
                // console.log($('body').html());
                $('script[charset=UTF-8]').each(function(i, elem) {
                    if( i == 1){
                        var scriptString = $(this).html()
                        script     = scriptString.substring(23, scriptString.length);
                        script     = script.substring(0 , script.length - 1);
                        var json   = JSON.parse(script);
                        data.script = json
                    }         
                });

                if(data.script != undefined){
                    var exercises           = data.script.exercises.all
                    var currentExerciseKey  = exercises[data.script.exercises.current].projector_key;
                    var iframeUrl           = null;
                    if(currentExerciseKey != null){
                       iframeUrl           = 'https://projector.datacamp.com/?auto_play=play&playback_rate=1&quality=auto&projector_key=' + currentExerciseKey;
                    }
                    data.iframeUrl    = iframeUrl;
                }

                return resolve(data)

            })
            .catch(function (err) {
                console.log(err)
            });
        
        });

        return promise;

    },















    saveExerciseDataInDB : async function(exerciseData){
        var newExercise          = exerciseData;
        newExercise.name         = replaceIllegalCharacters(newExercise.name)
        var exercise              = await new models.exercise(newExercise)
        try {
            //console.log('Saving Exercise in DB');
            if(exercise.type == 'webpage'){
                exercise.complete = true
            }

            if(exercise.type == 'video' || exercise.type == 'audio' || exercise.type == 'projector'){
                exercise.downloaded = false;
            }

            await exercise.save();
            return exercise;
        } catch (error) {
            return ({error : error})
        }        
    },



    markVideoAsDownloaded : async function(exerciseId){
        
        try {
            var exercise = await models.exercise.findById(exerciseId).exec()
            if(exercise.type != 'webpage'){
                exercise.downloaded = true
            }
            await exercise.save();
            return;

        } catch (error) {
            return ({error : error})
        }        
    },



    deleteExercisesFromDB : async function(exercisesToDelete){

        for (const [index, id] of exercisesToDelete.entries()) {
            try {
                await models.exercise.findByIdAndRemove(id).exec()
                //console.log('Exercise Delete : ' + id)
            } catch (error) {
                console.log(error.message);
                //return ({error : error})
            }  
        }

        return
    },





    saveExerciseAddressInDatabase : async function(link){
        try {

            var query = {link : 'https://www.datacamp.com' + link }
            console.log(query)
            var course = await models.course.findOne(query)
            .populate({
                path    : 'chapters',
                populate: [
                    { path:  'exercises' , model: 'Exercise'}
                ]
            })
            .exec()


            for (const [index, chapter] of course.chapters.entries()) { 

                for (const [index, exercise] of chapter.exercises.entries()) { 
                    
                    var exerciseAddressData         = {};
                    exerciseAddressData.link        = exercise.link
                    exerciseAddressData.address     = '../.' + process.env.COURSE_FOLDER + '/' + course.name + '/' + (chapter.number + 1) + ' - ' + chapter.name + '/';
                    
                    if(exercise.type == 'webpage'){
                        exerciseAddressData.name        = (exercise.number + 1) + ' - ' + exercise.name + '.html';
                        exerciseAddressData.type        = 'webpage';
                    
                    }
                    else if(exercise.type == 'video'){
                        exerciseAddressData.name        = (exercise.number + 1) + ' - ' + exercise.name + '.html';
                        exerciseAddressData.type        = 'video';

                        var videoData                       = {}
                        const name                          = (exercise.number + 1) + ' - ' + exercise.name + '.mp4';
                        videoData.name                      = name
                        videoData.videoLink                 = exercise.course + '/video/' + name
                        exerciseAddressData.videoData       = videoData
                    
                    } 
                    else{                   
                        
                        exerciseAddressData.name            = (exercise.number + 1) + ' - ' + exercise.name + '.html';
                        exerciseAddressData.type            = 'projector';
                        var projectorData                   = {}

                        if(exercise.projectorData.type == 'audio'){
                            const name                          = (exercise.number + 1) + ' - ' + exercise.name + '.mp3';
                            projectorData.name                  = name
                            projectorData.audioLink             = exercise.course + '/mp3/' + name
                            projectorData.type                  = 'audio';
                            exerciseAddressData.projectorData   = projectorData
                        }else{
                            const name                          = (exercise.number + 1) + ' - ' + exercise.name + '.mp4';
                            projectorData.name                  = name;
                            projectorData.videoLink             = exercise.course + '/video/' + name;
                            projectorData.type                  = 'video';
                            exerciseAddressData.projectorData   = projectorData;
                        }

                        // if(exercise.videoData !== undefined){
                        //     const name                          = (exercise.number + 1) + ' - ' + exercise.name + '.mp4';
                        //     exerciseAddressData.videoData           = {};
                        //     exerciseAddressData.videoData.name      = name;
                        //     exerciseAddressData.videoData.videoLink = exercise.videoData.videoLink;
                        // }


                    }
                    
                    
                    var exerciseAddress = new models.exerciseAddress(exerciseAddressData)
                    await exerciseAddress.save();
                    

                    var projectorAddressData         = {};
                    if(exerciseAddress.type == 'projector'){
                        
                        if(exerciseAddress.projectorData.type == 'audio'){
                            projectorAddressData.type        = 'audio';
                            projectorAddressData.name        = exerciseAddress.projectorData.name;
                            projectorAddressData.link        = exerciseAddress.projectorData.audioLink;
                        
                        }else{
                            projectorAddressData.type        = 'video';
                            projectorAddressData.name        = exerciseAddress.projectorData.name;
                            projectorAddressData.link        = exerciseAddress.projectorData.videoLink;
                        
                        }
                        
                        
                        projectorAddressData.address     = '../.' + process.env.COURSE_FOLDER + '/' + course.name + '/' + (chapter.number + 1) + ' - ' + chapter.name + '/';
                        var projectorAddress = new models.exerciseAddress(projectorAddressData)
                        await projectorAddress.save();

                    }else if(exerciseAddress.type == 'video'){

                        projectorAddressData.type        = 'video';
                        projectorAddressData.name        = exerciseAddress.videoData.name;
                        projectorAddressData.link        = exerciseAddress.videoData.videoLink;

                        projectorAddressData.address     = '../.' + process.env.COURSE_FOLDER + '/' + course.name + '/' + (chapter.number + 1) + ' - ' + chapter.name + '/';
                        var projectorAddress = new models.exerciseAddress(projectorAddressData)
                        await projectorAddress.save();
                    }


                }

            }

            

        } catch (error) {
            ({error : error})
            console.log(error)
        }
    },


    returnExercisesAddress: async function(link){
        
        try {
            var query = {link : 'https://campus.datacamp.com' + link }
            var result = await models.exerciseAddress.findOne(query)
            return result

        } catch (error) {
            ({error : error})
        }

    },



    returnExercisesMediaAddress: async function(link){
        
        try {
            var query = {link : link }
            var result = await models.exerciseAddress.findOne(query)
            return result

        } catch (error) {
            ({error : error})
        }

    },



}