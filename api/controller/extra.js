    console.log(content);

    await page.on('onResourceRequested', function(requestData) {
        console.info('Requesting', requestData.url);
     });

    page.property('plainText').then(function(content) {
         console.log(content);
    });



            // fs.writeFile('sample.html', content, (err) => {  
            //     // throws an error, you could also catch it here
            //     if (err) throw err;
            //     // success case, the file was saved
            //     console.log('Document saved!');
            // });





            // for (let j = 0; j < chapterData.exercises.length; j++) {

            //     let exercise = await new Exercise(chapterData.exercises[j])
            //     try {
            //         await exercise.save();
            //         exerciseIds.push(exercise._id)
            //     } catch (error) {
            //         console.log(error.message)   
            //     } 
            // }








            async function getExercisesVideoAndWebpageLinks(chapter){

                const baseAddress = '/Volumes/MyStuff/DataCamp-DL/course/'
                var options = {
                    urls: [],
                    directory: '',
                };
        
        
                options.directory  = baseAddress + chapter.name
                options.urls.push({url: chapter.exercises[1].link, filename: chapter.exercises[1].name + '.html'}) 
        
        
                if(chapter.exercises[0].type == 'video'){
        
                    const instance      = await phantom.create();
                    const page          = await instance.createPage();
                    var exerciseurl      = chapter.exercises[0].link
                
                    const status  = await page.open(exerciseurl);
                    const content = await page.property('content');
                    
            
                    // console.log(content)
                    page.on('onConsoleMessage' , function(data) {
                        console.log('Printing Data')
                        console.log(data);
                    });
            
                    
            
                    console.log('Analyzing Exercise page');
            
            
                    var iframeUrl = await page.evaluate(function() {
                        const elementsByTag = document.querySelectorAll('iframe');
                        return elementsByTag[0].getAttribute('src')
                    });
            
                    extractDownloadlinkFromIframe(iframeUrl)
        
                }else{
        
        
                    const instance      = await phantom.create();
                    const page          = await instance.createPage();
                    var exerciseurl      = chapter.exercises[1].link
            
                    const status  = await page.open(exerciseurl);
                    const content = await page.property('content');
                    
                    page.on('onConsoleMessage' , function(data) {
                        console.log('Printing Data')
                        console.log(data);
                    });
            
                    console.log('Analyzing Exercise page');
                    downloadExercise(options)
        
        
                }
        
        
                
                
                return
        
            }







            





    // links = document.querySelectorAll('a.course-block__link').map(function(link) {
    //     return link.getAttribute('href');
    // });

    // return links

    // var child = page.evaluate(function() {
    //     return [].map.call(document.querySelectorAll('a.course-block__link'), function(div) {
    //         return div.innerHTML;
    //     });
    // });

    // const totalLinks = document.querySelectorAll('a.course-block__link').length
    // for (i = 0; i < totalChapters; i++){ 

    // }



                    // return [].map.call(document.querySelectorAll('a.course-block__link'), function(link) {
                //     return link.getAttribute('href');
                // });
                




            // await page.evaluate(function() {

            //     const title       = document.title;
            //     const url         = document.URL;
            //     const description = document.querySelectorAll('.header-hero__description > p')[0].innerHTML
                
            //     var links = []
         
            //     const totalLinks = document.querySelectorAll('a.course-block__link').length;
            //     for (i = 0; i < totalLinks; i++){
            //         const link = document.querySelectorAll('a.course-block__link')[i].getAttribute('href')
            //         links.push(link)
            //     }

            //     console.log(links)
            //     var data = {};
            //     data.link              = url
            //     data.name              = title
            //     data.description       = description
            //     data.courselinks       = links
            //     return data 

            // }).then(function(data){
                  
            //    var track               = new Track(data);
            //    track.save()
            //    .then(function(){
            //         console.log('Track Saved')   
            //    })
            //    .catch(function(err){
            //         console.log(err)
            //    })
            //     instance.exit();
            // }).then(function(){
            //     console.log("Get Courses One by One")
            //     getTrackCoursesOneByOne(track);
            // })
            // .catch(function (err) {
            //     console.log(err);
            // });




            function getTrackObject(data){
        
                (async function() {
                    let track = await new Track(data)
                    return await track.save()
                })
               
            }
            // .then(function(){
                        
            // })





        // scrape(options)
        // .then((result) => {
        //     //console.log('Saved Page')
        //     return
        // })
        // .catch((err) => {
        //     //console.log(err)
        //     return
        // });














            // The options argument is optional so you can omit it
            progress(request(url), {
                // throttle: 2000,                    // Throttle the progress event to 2000ms, defaults to 1000ms
                // delay: 1000,                       // Only start to emit after 1000ms delay, defaults to 0ms
                // lengthHeader: 'x-transfer-length'  // Length header to use, defaults to content-length
            })
            .on('request',function(req){

                    console.log();
                    bar = new ProgressBar(chalk.green("    downloading [:bar] :percent [:t_downloaded]  TOTAL SIZE :t_size / TIME REMAINING :download_time :complet"), {
                    complete: '>',
                    incomplete: ' ',
                    width: 50,
                    total: 100
                    });

            })
            .on('progress', function (state) {
                // The state is an object that looks like this:
                // {
                //     percent: 0.5,               // Overall percent (between 0 to 1)
                //     speed: 554732,              // The download speed in bytes/sec
                //     size: {
                //         total: 90044871,        // The total payload size in bytes
                //         transferred: 27610959   // The transferred payload size in bytes
                //     },
                //     time: {
                //         elapsed: 36.235,        // The total elapsed seconds since the start (3 decimals)
                //         remaining: 81.403       // The remaining seconds to finish (3 decimals)
                //     }
                // }

                total_size = bytesToSize(state.size.total);
                remaining_time = convertTime(state.time.remaining);
                total_downloaded = bytesToSize(state.size.transferred);

                bar.update(state.percent,{
                    'complet' :  "",
                    't_size' : total_size,
                    'download_time' : remaining_time,
                    't_downloaded' : total_downloaded
                });

            })
            .on('error', function (err) {
                console.log();
                console.log(chalk.red("Error Downloading please try again !!"));
            })
            .on('end', function () {
                    bar.update(1,{
                    'complet' :  "\u2713",
                        't_size' : total_size,
                        'download_time' : remaining_time,
                        't_downloaded' : total_size
                    });
                    //console.log("\u2713");
                    console.log("\n");

                callback();
            })
            .pipe(fs.createWriteStream(apiPath));
















            var url = video.videoLink
                var bar;
                var fileUrl = url,
                    //base_path = argv.output ? files.getCurrentDirectoryBase() + path.sep + argv.output : files.getCurrentDirectoryBase(),
                    //edited to get current file with its chapter number
                    title = video.name,
                    apiPath = video.directory + '/' + video.name ,
                    total_size = 0,
                    total_downloaded = 0,
                    remaining_time = 0;

                if (fs.existsSync( video.directory + '/' + video.name)){
                    console.log('File exists : ' + video.name)
                }else{
                    // The options argument is optional so you can omit it
                    var download = await progress(request(url), {
                        // throttle: 2000,                    // Throttle the progress event to 2000ms, defaults to 1000ms
                        // delay: 1000,                       // Only start to emit after 1000ms delay, defaults to 0ms
                        // lengthHeader: 'x-transfer-length'  // Length header to use, defaults to content-length
                    })


                    await download.on('request',function(req){
                            console.log();
                            bar = new ProgressBar(chalk.green("Downloading [:bar] :percent [:t_downloaded]  TOTAL SIZE :t_size / TIME REMAINING :download_time :complet"), {
                            complete: '>',
                            incomplete: ' ',
                            width: 50,
                            total: 100
                            });
                    })



                    await download.on('progress', function (state) {
                        total_size = bytesToSize(state.size.total);
                        remaining_time = convertTime(state.time.remaining);
                        total_downloaded = bytesToSize(state.size.transferred);

                        bar.update(state.percent,{
                            'complet' :  "",
                            't_size' : total_size,
                            'download_time' : remaining_time,
                            't_downloaded' : total_downloaded
                        });
                    })


                    await download.on('error', function (err) {
                        console.log();
                        console.log(chalk.red("Error Downloading please try again !!"));
                    })


                    await download.on('end', function () {
                            // bar.update(1,{
                            // 'complet' :  "\u2713",
                            //     't_size' : total_size,
                            //     'download_time' : remaining_time,
                            //     't_downloaded' : total_size
                            // });
                            //console.log("\u2713");
                            console.log("\n");
                        
                        return
                    })


                    //await download.pipe(fs.createWriteStream(apiPath));
                }










    // function download(uri,dest){ 

    //     return new Promise(function(resolve, reject){ 
    //         let req = request(uri); 
    //         progress(req) 
    //         .on('error', function (err) { 
    //             reject(err); 
    //         }) 
    //         .on('end', function () { 
    //             resolve() 
    //         }) 
    //         .pipe(fs.createWriteStream(dest)); 
    //     }); 
    // }




    // function download(uri,dest){ 
        
    //     return new Promise(function(resolve, reject){ 
    //         let req = request(uri); 
    //         progress(req) 
    //         .on('error', function (err) { 
    //             reject(err); 
    //         }) .on('end', function () { 
    //             resolve() 
    //         }); 
    //         req.pipe(fs.createWriteStream(dest)); 
    //     }); 
    // }









    async function downloadVideo(videosData){

        for (const [index, video] of videosData.entries()) { 
            
            var url = video.videoLink
            
            var options = {
                directory: video.directory,
                filename: video.name
            }
            
            if (fs.existsSync( video.directory + '/' + options.filename)){
                console.log('File exists : ' + options.filename)
            }else{
                await download(url, options, function(err){
                    if (err) {
                        console.log(err)   
                    }else{
                        console.log('Downloaded video : ' + url);
                        return
                    }
                })

                console.log('lalalaalalalalaalalalalallallalalalalalalal')
            }
            
        }


        //return
    }
















    var pdfLinks = function(req , res , next){

        (async function() {
           
            console.log('Getting exercise page for pdf links .... wait');

            const instance  = await phantom.create();
            const page      = await instance.createPage();
            
            const url       = 'https://campus.datacamp.com/courses/intro-to-python-for-data-science/chapter-2-python-lists?ex=2'
            const status    = await page.open(url);
            const content   = await page.property('content');
        
            page.on('onConsoleMessage' , function(data) {
                console.log(data);
            });


            console.log('Analyzing Exercise page : ' + url);

            let data = await page.evaluate(function() {

                console.log('Getting pdf links');
                var description = document.querySelectorAll('body > script')[1].innerHTML
                description = description.substring(23, description.length);
                
                //console.log(description)
                console.log(typeof description)

                var json = JSON.parse(description);
                console.log(JSON.stringify(json))
            })

            
              
            await instance.exit();



          })();
    }















    console.log('\n\n\n')
    console.log('2-Getting course data for : ' + 'https://www.datacamp.com' + link);
    
    const courseExist = await courseController.searchCourseInDbByLink(link)
    if(courseExist == undefined){
        await getCourseData(link , trackExist._id)
    }

    else if(courseExist !== undefined && courseExist.complete == true){
        let result = await trackController.saveCourseIdInTrack(courseExist._id , trackExist._id);
        if(result){
            await courseController.incrementCourseTrackCounter(courseExist._id)
        }

    }
    
    else if(courseExist !== undefined && courseExist.complete == false){
        
        let track = await trackController.getFullTrackData(req.params.search_track)

        var courseToDelete      = {}
        var chaptersToDelete    = []
        var exercisesToDelete   = []

        if(track.courses.length){

            track.courses.forEach(course => {
                if(course._id.equals(courseExist._id)){
                    courseToDelete = course
                }
            });

            
            courseToDelete.chapters.forEach(chapter => {
                chapter.exercises.forEach(exercise => {
                    exercisesToDelete.push(exercise._id);
                });

                chaptersToDelete.push(chapter._id);
            });

            await exerciseController.deleteExercisesFromDB(exercisesToDelete);
            await chapterController.deleteChaptersFromDB(chaptersToDelete);
            await courseController.deleteCourseFromDb(courseToDelete)
        }

        await getCourseData(link , trackExist._id)

    }


















    var searchExerciseOnlineByLink =  function(link){


        var promise =  new Promise(function(resolve, reject){ 
            pool.use(async (instance) => {
                
                const page          = await instance.createPage();
                var exerciseurl     = link
            
                const status  = await page.open(exerciseurl);
                const content = await page.property('content');

                page.on('onConsoleMessage' , function(data) {
                    console.log(data);
                });

                console.log('Analyzing Exercise page : ' + exerciseurl);

                var data = await page.evaluate(function() {
                    
                    var dataObj = {}
                    
                    const elementsByTag = document.querySelectorAll('iframe');
                    dataObj.iframeUrl   = elementsByTag[0].getAttribute('src')

                    var script = document.querySelectorAll('body > script')[1].innerHTML
                    script     = script.substring(23, script.length);
                    var json   = JSON.parse(script);
                    console.log(dataObj.iframeUrl);
                    dataObj.script = json
                    return dataObj

                });

                return resolve(data)

            })
        
        });

        return promise;

    }



    











    // extractDownloadLinkForVideo : async function(link){

    //     const instance      = await phantom.create();
    //     const page          = await instance.createPage();
    //     var iframeUrl       = link
    
    //     const status        = await page.open(iframeUrl);
    //     const content       = await page.property('content');
        

    //     await page.on('onConsoleMessage' , function(data) {
    //         console.log(data);
    //     });



    //     console.log('Analyzing Video page : ' + iframeUrl);
    //     const videoUrl = await page.evaluate(function() {
    //         var data = document.getElementById('videoData').getAttribute('value')
    //         //console.log(JSON.stringify(document.querySelectorAll('html')[0].innerHTML))
    //         var json = JSON.parse(data);
    //         var url = json.video_mp4_link.replace("//", "https://");
    //         return url
    //     });

    //     await instance.exit()
    //     return videoUrl

    // },


    // searchExerciseOnlineByLink : async function(link){

    //     const instance      = await phantom.create();
    //     const page          = await instance.createPage();
    //     var exerciseurl     = link
    
    //     const status  = await page.open(exerciseurl);
    //     const content = await page.property('content');

    //     page.on('onConsoleMessage' , function(data) {
    //         console.log(data);
    //     });

    //     console.log('Analyzing Exercise page : ' + exerciseurl);

    //     var data = await page.evaluate(function() {
            
    //         var dataObj = {}
            
    //         const elementsByTag = document.querySelectorAll('iframe');
    //         dataObj.iframeUrl   = elementsByTag[0].getAttribute('src')

    //         var script = document.querySelectorAll('body > script')[1].innerHTML
    //         script     = script.substring(23, script.length);
    //         var json   = JSON.parse(script);

    //         dataObj.script = json
    //         return dataObj

    //     });

    //     await instance.exit()
    //     return data;
    // },













    // extractDownloadLinkForVideo : async function(link){

    //     var promise =  new Promise(function(resolve, reject){ 
            
    //         pool.use(async (instance) => {
                
    //             const page          = await instance.createPage();
    //             var   iframeUrl       = link
    //             const status        = await page.open(iframeUrl);
    //             const content       = await page.property('content');
                
        
    //             await page.on('onConsoleMessage' , function(data) {
    //                 console.log(data);
    //             });
        
    //             console.log('Analyzing Video page : ' + iframeUrl);
    //             const videoUrl = await page.evaluate(function() {
    //                 var data = document.querySelectorAll('input')[0].getAttribute('value');
                    
    //                 //var data = document.getElementById('videoData').getAttribute('value');
    //                 //console.log(JSON.stringify(document.getElementById('videoData')))
    //                 //console.log(JSON.stringify(document.querySelectorAll('input')[0]));

    //                 var json = JSON.parse(data);
    //                 var url = json.video_mp4_link.replace("//", "https://");
    //                 return url;
    //             });
        

    //             if(videoUrl == null){
    //                 console.log('Null value content');
    //                 console.log(content);
    //             }

    //             return resolve(videoUrl)

    //         })
        
    //     });

    //     return promise;

    // },









    // Returns a generic-pool instance
const pool = createPhantomPool({
    max: 10, // default
    min: 2, // default
    // how long a resource can stay idle in pool before being removed
    idleTimeoutMillis: 30000, // default.
    // maximum number of times an individual resource can be reused before being destroyed; set to 0 to disable
    maxUses: 50, // default
    // function to validate an instance prior to use; see https://github.com/coopernurse/node-pool#createpool
    validator: () => Promise.resolve(true), // defaults to always resolving true
    // validate resource before borrowing; required for `maxUses and `validator`
    testOnBorrow: true, // default
    // For all opts, see opts at https://github.com/coopernurse/node-pool#createpool
    phantomArgs: [[ '--ignore-ssl-errors=true', 
                    '--disk-cache=true' , 
                    '--local-to-remote-url-access=true',
                    '--ssl-protocol=any'
                ], {
    //   logLevel: 'debug',
    }], // arguments passed to phantomjs-node directly, default is `[]`. For all opts, see https://github.com/amir20/phantomjs-node#phantom-object-api
  })





  if(videoData.url !== undefined){
    exercise.videoLink                      = videoData.url;
}else{
    exercise.videoLink                      = null;
    exercise.type                           = 'projector';
    exercise.projectorData.audioLink        = videoData.audioLink;
    exercise.projectorData.projectorLink    = videoData.projectorLink;
}









if (exercise.type == 'video' && exercise.videoLink !== undefined && exercise.videoLink !== null){
    var video = {}
    video.videoLink = exercise.videoLink
    video.name      = (exercise.number + 1) + ' - ' + exercise.name + '.mp4'
    video.directory = directory
    videosData.push(video)
}
else if (exercise.type == 'webpage'){
    options.urls.push({url: exercise.link, filename: (exercise.number + 1) + ' - ' + exercise.name + '.html'})    
}
else if (exercise.type == 'projector'){
    options.urls.push({url: exercise.projectorData.projectorLink, filename: (exercise.number + 1) + ' - ' + exercise.name + '.html'})    
    
    var video = {}
    video.videoLink = exercise.projectorData.audioLink
    video.name      = (exercise.number + 1) + ' - ' + exercise.name + '.mp3'
    video.directory = directory
    videosData.push(video)
    // await downloadExercise(options)
    // await downloadFile.videoDownload(video);
}





        // if(videoData.url !== undefined){
        //     exercise.videoLink                      = videoData.url;
        // }else{
        //     exercise.videoLink                      = null;
        //     exercise.type                           = 'projector';
        //     exercise.projectorData.audioLink        = videoData.audioLink;
        //     exercise.projectorData.projectorLink    = videoData.projectorLink;
        // }





        for (const [index, chapter] of course.chapters.entries()) { 

            for (const [index, exercise] of chapter.exercises.entries()) { 
                var exerciseAddressData         = {};
                exerciseAddressData.link        = exercise.link
                exerciseAddressData.address     = '../.' + process.env.COURSE_FOLDER + '/' + course.name + '/' + (chapter.number + 1) + ' - ' + chapter.name + '/';
                
                if(exercise.type == 'webpage'){
                    exerciseAddressData.name        = (exercise.number + 1) + ' - ' + exercise.name + '.html';
                    exerciseAddressData.type        = 'webpage';
                
                }
                else if(exercise.type == 'video'){
                    exerciseAddressData.name        = (exercise.number + 1) + ' - ' + exercise.name + '.mp4';
                    exerciseAddressData.type        = 'video';
                
                } 
                else{
                    exerciseAddressData.name            = (exercise.number + 1) + ' - ' + exercise.name + '.html';
                    exerciseAddressData.type            = 'projector';
                    var projectorData                   = {}
                    const name                          = (exercise.number + 1) + ' - ' + exercise.name + '.mp3';
                    projectorData.name                  = name
                    projectorData.audioLink             = '/mp3/' + name
                    exerciseAddressData.projectorData   = projectorData
                }
                
                var exerciseAddress = new models.exerciseAddress(exerciseAddressData)
                //console.log(exerciseAddress);
                await exerciseAddress.save();

                if(exerciseAddress.type == 'projector'){
                    var projectorAddressData         = {};
                    projectorAddressData.type        = 'audio';
                    projectorAddressData.name        = exerciseAddress.projectorData.name;
                    projectorAddressData.link        = exerciseAddress.projectorData.audioLink;
                    projectorAddressData.address     = '../.' + process.env.COURSE_FOLDER + '/' + course.name + '/' + (chapter.number + 1) + ' - ' + chapter.name + '/';
                
                    var audioAddress = new models.exerciseAddress(projectorAddressData)
                    await audioAddress.save();
                }


            }

        }





                        // var json = JSON.parse($(this).attr('value'));

                // if(json.audio_link !== undefined && json.audio_link !== null){
                //     json.audio_link = 'http://localhost:3001'+result.projectorData.audioLink;
                // }

                // if(i == 0){
                //     $(this).attr('value' , JSON.stringify(json));
                // }





                //else if(result.type == 'audio'){

                // var json = JSON.parse($(this).attr('value'));

                // if(json.audio_link !== undefined && json.audio_link !== null){
                //     json.audio_link = 'http://localhost:3001'+result.projectorData.audioLink;
                // }

                // if(i == 0){
                //     $(this).attr('value' , JSON.stringify(json));
                // }
        //}