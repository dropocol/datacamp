var     phantom             = require('phantom');
const   cheerio             = require('cheerio')
var     rp                  = require('request-promise');

var     trackController = require('./trackController');
var     courseController = require('./courseController');
var     chapterController = require('./chapterController');
var     exerciseController = require('./exerciseController');


function replaceIllegalCharacters(str) {
    var string = str
    string =  string.replace(new RegExp('/ ', 'g'), '-');
    string =  string.replace(new RegExp('/', 'g'), '-');
    string =  string.replace(new RegExp('&amp;', 'g'), 'and');
    string =  string.replace(new RegExp(': ', 'g'), '-');
    string =  string.replace(new RegExp(':', 'g'), '-');
    return string;
}


module.exports = {


    searchCourseInDbByLink : async function(link){
        try {
            var query = { link : 'https://www.datacamp.com' + link}
            console.log(query);
            const result = await models.course.findOne(query).exec()
            return result

        } catch (error) {
            return ({error : error})
        }
    },

    getAllCourseLinksInDB : async function(link){
        try {   
            var data  = [];
            var query = {complete : true};
            var result = await models.course.find(query).exec()

            for (const [index, course] of result.entries()) { 
                var courseData   = {}
                courseData.name  = course.name;
                courseData.link  = course.link.substring(24, course.link.length);
                data.push(courseData);
            }

            return data

        } catch (error) {
            ({error : error})
        }
    },


    searchCourseOnlineByLink : async function(link){

        var courseurl     = 'https://www.datacamp.com' + link
        const instance  = await phantom.create();
        const page      = await instance.createPage();
        

        const status  = await page.open(courseurl);
        const content = await page.property('content');
        

        page.on('onConsoleMessage' , function(data) {
            console.log(data);
        });

        console.log('Analyzing Course page : ' + link );

        let data = await page.evaluate(function() {

            console.log('Getting data from Course page');
            const courseDescription     = document.querySelectorAll('.course__description')[0].innerHTML;
            const courseId              = document.getElementById('course_id').getAttribute('value');
            var courseName              = document.querySelectorAll('.header-hero__title')[0].innerHTML;
            //courseName                  = replaceIllegalCharacters(courseName)

            var courseData              = {};
            courseData.link             = document.URL;
            courseData.courseId         = courseId.toString();
            courseData.name             = courseName;
            courseData.description      = courseDescription;
            courseData.chapters         = []
            // console.log(courseData.name)

            const totalChapters = document.querySelectorAll('.chapter').length

            for (i = 0; i < totalChapters; i++){

                const container                 = document.querySelectorAll('.chapter')[i];

                const chapterNumber             = document.querySelectorAll('.chapter__title-wrapper > .chapter__number > .chapter-number')[i].innerHTML;
                var chapterTitle                = document.querySelectorAll('.chapter__title-wrapper > .chapter__title')[i].innerHTML;
                var chapterDescription          = document.querySelectorAll('.chapter > .chapter__description')[i].innerHTML;
                
                chapterTitle                    = chapterTitle.trim();
                //chapterTitle                    = replaceIllegalCharacters(chapterTitle)

                chapterDescription              = chapterDescription.trim();

                const totalChapterExercises     = container.querySelectorAll('.chapter__exercises > .chapter__exercise').length
                const chapterExercises          = container.querySelectorAll('.chapter__exercises > .chapter__exercise')

                var chapterData             = {};
                chapterData.link             = document.URL;
                chapterData.number          = i;
                chapterData.name            = chapterTitle;
                chapterData.description     = chapterDescription
                chapterData.exercises       = []

                // console.log(JSON.stringify(courseData))
                
                for (j = 0; j < totalChapterExercises; j++){
                    const exerciseLink   = chapterExercises[j].querySelectorAll('a')[0].getAttribute('href');
                    var exerciseName     = chapterExercises[j].querySelectorAll('a > h5')[0].innerHTML;
                    //exerciseName         = replaceIllegalCharacters(exerciseName)
                    
                    const exerciseType   = chapterExercises[j].querySelectorAll('.chapter__exercise-icon > img')[0].getAttribute('alt');

                    var exercise        = {};
                    exercise.number     = j;
                    exercise.name       = exerciseName;
                    exercise.link       = exerciseLink;
                    exercise.type       = ''

                    if(exerciseType.indexOf('Icon exercise video') == -1){
                        exercise.type  = 'webpage'
                    }else {
                        exercise.type  = 'video'
                    }

                    // console.log(JSON.stringify(exercise))
                    //console.log('Gonna push exercise in the array');
                    chapterData.exercises.push(exercise)

                }

                //  console.log(JSON.stringify(chapterData))
                courseData.chapters.push(chapterData);
            }

            return courseData;

        })
        
        await instance.exit();
        return data
    },




    saveCourseDataInDB : async function(courseData){
        let course       = await new models.course(courseData)
        course.name      = replaceIllegalCharacters(course.name)
        try {
            console.log('Saving Course page in DB');
            await course.save();
            return course;
        } catch (error) {
            return ({error : error})
        }        
    },



    markCourseAsComplete : async function(courseId){
        try {
            const course                = await models.course.findById(courseId).exec()
            course.complete             = true;
            await course.save();
            return

        } catch (error) {
            return ({error : error})
        }      
    },



    incrementCourseTrackCounter : async function(courseId){
        try {
            const course                = await models.course.findById(courseId).exec()
            course.connectedToTracks    = course.connectedToTracks + 1;
            await course.save();
            return

        } catch (error) {
            return ({error : error})
        }      
    },


    coursesToDeleteFromDB : async function(courses){
        var coursesToDelete = [];
        for (const [index, course] of courses.entries()) {
            if(course.complete == false){
                coursesToDelete.push(course)
            }
        }
        return coursesToDelete
    },



    deleteCourseFromDb : async function(courseToDelete){
        try {
            var course = await models.course.findById(courseToDelete).exec()
            if(course != undefined && course.connectedToTracks >= 1){
                course.connectedToTracks = course.connectedToTracks - 1;
                if(course.connectedToTracks == 0){
                    await course.remove()
                }else{
                    await course.save()
                }
            }
            return 
        } catch (error) {
            console.log(error.message);
            return 
        }  
    },



    deleteCourseFromDbByLink : async function(link){

        var query = { link : 'https://www.datacamp.com/courses/' + link}
        try {

            var course = await models.course.findOne(query)
            .populate({
                path    : 'chapters'
            })
            .exec()

            //console.log(course)

            var chaptersToDelete    = [];
            var exercisesToDelete   = [];

            course.chapters.forEach(chapter => {
                chapter.exercises.forEach(exercise => {
                    exercisesToDelete.push(exercise);
                });
                chaptersToDelete.push(chapter._id);
            }); 

            await exerciseController.deleteExercisesFromDB(exercisesToDelete);
            await chapterController.deleteChaptersFromDB(chaptersToDelete);

            await course.remove();

        } catch (error) {
            console.log(error.message);
            return 
        }  
    },


    getFullCourseData : async function(id){

        try {
            var course = await models.course.findById(id)
            .populate({
                path    : 'chapters',
                populate: [
                    { path:  'exercises' , model: 'Exercise'}
                ]
            })
            .exec()
            return course;

        } catch (error) {
            ({error : error})
        }
    },



    getFullCourseDataByLink : async function(link){

        try {
            var query = {link : 'https://www.datacamp.com/courses/'+link }
            var course = await models.course.findOne(query)
            .populate({
                path    : 'chapters',
                populate: [
                    { path:  'exercises' , model: 'Exercise'}
                ]
            })
            .exec()
            return course;

        } catch (error) {
            ({error : error})
        }
    },


    getCourseChaptersInDB : async function(link){
        try {   
            var data  = {};
            //var query = {complete : true};
            var query = {link : 'https://www.datacamp.com/courses/' + link }
            var course = await models.course.findOne(query)
            .populate({
                path    : 'chapters',
                populate: [
                    { path:  'exercises' , model: 'Exercise'}
                ]
            })
            .exec()


            data.courseName = course.name;
            data.chapters   = [];

            for (const [index, chapter] of course.chapters.entries()) { 
                var chapterData   = {}
                chapterData.name  = chapter.name;

                var exercises = [];
                for (const [index, exercise] of chapter.exercises.entries()) { 
                    var exerciseData = {};
                    exerciseData.name = exercise.name;
                    exerciseData.link = exercise.link.substring(27, exercise.link.length);
                    exercises.push(exerciseData);
                }

                chapterData.exercises = exercises
                data.chapters.push(chapterData);
            }

            return data

        } catch (error) {
            ({error : error})
        }
    },



    getAllCoureLinksOnline : async function(){

        var link = 'https://www.datacamp.com/courses/all'

        var promise =  new Promise(function(resolve, reject){ 
            rp(link)
            .then(function (htmlString) {

                var $ = cheerio.load(htmlString);
                var courses = [];

                $('.course-block__link').each(function(i, elem) {
                    console.log($(this).attr('href'));
                    courses.push($(this).attr('href'));
                });

                return resolve(courses)

            })
            .catch(function (err) {
                console.log(err)
            });
        
        });

        return promise;
    }




}
