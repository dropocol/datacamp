const   fs          = require('fs');
var     mv          = require('mv');


function deleteFolderRecursive(path){
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
        return 
    }
}


async function moveSingleFile(source , destination){
    var promise =  new Promise(function(resolve, reject){ 
        mv(source, destination, {mkdirp: true}, function(err) {
            return resolve()
        }); 
    });
    return promise;
}




async function moveFiles(source , destination){
        
    for (const file of fs.readdirSync(source)) { 
        const fileSource        = source + file
        const fileDestination   = destination + file
        await moveSingleFile(fileSource , fileDestination)
    }

}


async function moveDirectory(source , destination){
    
    var promise =  new Promise(function(resolve, reject){ 
        mv(source, destination, {mkdirp: true}, function(err) {
            return resolve()
        });
    })

    return promise;
}



module.exports = {

    deleteFolderRecursive : async function(path){
        return deleteFolderRecursive(path)
    },


    renameFiles : async function(absoluteAddress){
        
        var promise =  new Promise(function(resolve, reject){ 

            var tempFolders  = ['css' , 'js']
            var counter      = 0

            for (const path of tempFolders) { 
                
                var currentPath = absoluteAddress + 'temp/' + path;

                for (const file of fs.readdirSync(currentPath)) { 

                    if(file.indexOf('style-') == 0 && path == 'css'){
                        var oldName =  currentPath + '/' + file
                        var newName =  currentPath + '/' + 'style.css'
                        fs.rename( oldName , newName , function(err) {
                            if ( err ) console.log('ERROR: ' + err);
                            counter ++;
                            if(counter == 2){
                                return resolve()
                            }
                        });
                    }

                    if(file.indexOf('client-') == 0 && path == 'js'){
                        var oldName =  currentPath + '/' + file
                        var newName =  currentPath + '/' + 'client.js'
                        fs.rename( oldName , newName , function(err) {
                            if ( err ) console.log('ERROR: ' + err);
                            counter ++;
                            if(counter == 2){
                                return resolve()
                            }
                        });
                    }

                }
            }
        });

        return promise
    },



    moveFilesToPublic : async function(absoluteAddress){
        
            var paths = ['css' , 'js' , 'fonts' , 'images']

            for (const [index , path] of paths.entries()) { 
                var sourse      = absoluteAddress + 'temp/' + path;
                var destination = absoluteAddress + path;
                await moveDirectory(sourse , destination);
            } 
            
    },


    moveAssetsToCourseMainDir : async function(mainSource , mainDestination){

        
        var paths = ['fonts/' , 'images/'];
        
        for (const [index , path] of paths.entries()) { 
            const source        = mainSource + path
            const destination   = mainDestination + path
            
            if( fs.existsSync(source)) {
                await moveFiles(source , destination);
                fs.rmdirSync(source);
            }
        }

    },



    moveFilesToChapterMainDir : async function(mainSource , mainDestination){

        var paths = ['temp/'];        
        for (const [index , path] of paths.entries()) { 
            const source        = mainSource + path
            const destination   = mainDestination
            
            if( fs.existsSync(source)) {
                await moveFiles(source , destination);
                fs.rmdirSync(source);
            }
        }

    }




}