'use strict';

const   util        = require('util');
var     request     = require('request');
var     async       = require('async');
var     phantom     = require('phantom');
const   fs          = require('fs');
var     scrape      = require('website-scraper');
var     download    = require('download-file')
var     progress    = require('request-progress');
var     ProgressBar = require('progress'); 
var     chalk       = require('chalk');
var     path        = require("path");

var     rp          = require('request-promise');
var     http        = require('http'),
        url         = require('url');

var     trackController         = require('./trackController');
var     courseController        = require('./courseController');
var     chapterController       = require('./chapterController');
var     exerciseController      = require('./exerciseController');
var     fileController          = require('./fileController');
var     downloadFile            = require('./download');


var totalVideo = 0



var downloadController = function (){


    var downloadTrack =  async function(req , res , next){

        var search = '';

        if(req.params.search_track == undefined){
            return res.json({
                message : 'Please provide a link'
            })
        }else{
            search = req.params.search_track
        }


        var track = await trackController.getFullTrackData(search)
        res.json({
            track : track
        })

        if(track != undefined){
            downloadCourses(track)
        }
    }




    async function downloadCourses(track){

        await downloadResourceFiles();
        for (const [index, course] of track.courses.entries()) { 
            
            var courseName        = course.name;
            var courseDir         = process.env.COURSE_FOLDER + '/' + courseName ;

            if (!fs.existsSync(courseDir)){
                fs.mkdirSync(courseDir);
            }

            await downloadCourse(course ,  courseDir)

        }

        console.log('Downloaded all files :)');

    }



    async function downloadResourceFiles(){ 
        
        const absoluteAddress = path.resolve("./") + '/public/'
        var folders    = ['css' , 'js' , 'fonts' , 'images' , 'temp']

        for (const path of folders) { 
            var currentPath = absoluteAddress + path;
            await fileController.deleteFolderRecursive(currentPath)
        }


        var options = {
            urls: [],
            prettifyUrls : true,
            directory: absoluteAddress + '/temp',
        };

        //Any url with exercise page to download latest assets
        var exampleUrl              = 'https://campus.datacamp.com/courses/intro-to-python-for-data-science/chapter-1-python-basics?ex=2'
        var exampleProjectorUrl     = 'https://projector.datacamp.com/?auto_play=play&playback_rate=1&quality=auto&projector_key=course_1946_f05d06ad7807cf476fdb5f674174c9d5'

        options.urls.push({url: exampleUrl, filename: 'example.html'})    
        options.urls.push({url: exampleProjectorUrl, filename: 'example2.html'})  
        await downloadExercise(options);
       
        await fileController.renameFiles(absoluteAddress);
        await fileController.moveFilesToPublic(absoluteAddress)
        await fileController.deleteFolderRecursive(absoluteAddress + '/temp');

        return
    }






    async function downloadCourse(course , courseDir){ 
    
        var dir = courseDir.replace(".", "");
        
        const absoluteAddress = path.resolve("./") + dir
        console.log(absoluteAddress)

        for (const [index, chapter] of course.chapters.entries()) { 
            await downloadChapter(course.courseId , chapter , absoluteAddress)
        }

        return

    }

    async function downloadChapter(courseId , chapter , absoluteAddress){
        
        var directory = absoluteAddress + '/' + (chapter.number + 1) + ' - ' + chapter.name
        var options = {
            urls: [],
            prettifyUrls : true,
            sources: [
                {selector: 'img', attr: 'src'}
            ],
            directory: directory + '/temp',
        };

        var videosData = []

        for (const [index, exercise] of chapter.exercises.entries()) { 


            if (exercise.type == 'projector'){
                
                var video = {}
                var link = '';

                video.id         = exercise.id;
                video.downloaded = exercise.downloaded;
                if(exercise.projectorData !== undefined){
                    if(exercise.projectorData.type == 'video'){
                        video.videoLink = exercise.projectorData.videoLink;
                        video.name      = (exercise.number + 1) + ' - ' + exercise.name + '.mp4'
                        video.directory = directory
                        link            = exercise.projectorData.projectorLink;
                    
                    }else{
                        video.videoLink = exercise.projectorData.audioLink;
                        video.name      = (exercise.number + 1) + ' - ' + exercise.name + '.mp3'
                        video.directory = directory
                        link            = exercise.projectorData.projectorLink;
                    
                    }
                
                }else if(exercise.videoData !== undefined){
                    video.videoLink = exercise.videoData.videoLink;
                    video.name      = (exercise.number + 1) + ' - ' + exercise.name + '.mp4'
                    video.directory = directory
                    link            = exercise.videoData.projectorLink;
                
                } 


                //if(exercise.downloaded == false){
                    videosData.push(video)
                //}

                var fileName = (exercise.number + 1) + ' - ' + exercise.name + '.html'
                var fileDir  = directory + '/' + fileName;
                if (!fs.existsSync(fileDir)){
                    options.urls.push({url: link, filename: fileName}) 
                }
                
            
            }else if (exercise.type == 'video'){
                
                var video = {}
                var link = '';
                
                video.id         = exercise.id;
                video.downloaded = exercise.downloaded;
                
                video.videoLink = exercise.videoData.videoLink;
                video.name      = (exercise.number + 1) + ' - ' + exercise.name + '.mp4'
                video.directory = directory
                link            = exercise.videoData.projectorLink;

                //if(exercise.downloaded == false){
                    videosData.push(video)
                //}
                
                var fileName = (exercise.number + 1) + ' - ' + exercise.name + '.html'
                var fileDir  = directory + '/' + fileName;
                if (!fs.existsSync(fileDir)){
                    options.urls.push({url: link, filename: fileName}) 
                }


            }else if(exercise.type == 'webpage'){
                
                var fileName = (exercise.number + 1) + ' - ' + exercise.name + '.html';
                var fileDir  = directory + '/' + fileName;
                if (!fs.existsSync(fileDir)){
                    options.urls.push({url: exercise.link, filename: fileName}) 
                }
            
            }

        }



        if(options.urls.length > 0){
            await downloadExercise(options)

            const filesSource      = absoluteAddress + '/' + (chapter.number + 1) + ' - ' + chapter.name + '/'
            const filesDestination = absoluteAddress + '/' + (chapter.number + 1) + ' - ' + chapter.name + '/'
            await fileController.moveFilesToChapterMainDir(filesSource , filesDestination)
    
            const assetSource      = absoluteAddress + '/' + (chapter.number + 1) + ' - ' + chapter.name + '/'
            const assetDestination = absoluteAddress + '/'
            await fileController.moveAssetsToCourseMainDir(assetSource , assetDestination)
        }
 
        
        await downloadFile.PdfDownload(chapter , directory)       

        for (const [index, video] of videosData.entries()) { 
            await downloadFile.videoDownload(video);
            await exerciseController.markVideoAsDownloaded(video.id);
        }

        return
    }





    async function downloadExercise(options){ 

        if (fs.existsSync(options.directory)){
            console.log('Folder exists')
            return
        }

        try {
            var result = await scrape(options)
            console.log('Saved Page')
            return
        } catch (error) {
            console.log(error.message)   
            return
        }
        
    }



    var downloadSingleCourse = async function(req , res , next){
        var search  = req.params.search_course
        let course = await courseController.getFullCourseDataByLink(search);

        res.json({course : course});

        if(course){
            var link        = course.link.substring(24 , course.link.length);
            
            var courseDir   = process.env.COURSE_FOLDER + '/' + course.name;

            if (!fs.existsSync(courseDir)){
                fs.mkdirSync(courseDir);
            }

            console.log('Downloading Course : ' + link)
            await downloadCourse(course , courseDir);

            //await downloadResourceFiles();
        }

    }




    var downloadAllCourses = async function(req , res , next){


        console.log('Getting Links for all courses');
        res.end();

        const jsAddress = path.resolve("./") + '/public/js'
        if (!fs.existsSync(jsAddress)){
            await downloadResourceFiles();
        }

        var courseLink = await courseController.getAllCoureLinksOnline();

        for (const link of courseLink) {

            const newLink = link.replace('/courses/' , '');
            let course = await courseController.getFullCourseDataByLink(newLink);
            if(course){
                var courseLink        = course.link.substring(24 , course.link.length);
                var courseDir         = process.env.COURSE_FOLDER + '/' + course.name;
    
                if (!fs.existsSync(courseDir)){
                    fs.mkdirSync(courseDir);
                }
    
                console.log('Downloading Course : ' + courseLink)
                await downloadCourse(course , courseDir);
            }
        }

        console.log('Downloaded all files :)');


    }





    return {
        downloadTrack           :       downloadTrack,
        downloadSingleCourse    :       downloadSingleCourse,
        downloadAllCourses      :       downloadAllCourses
    };

};


module.exports = downloadController;






