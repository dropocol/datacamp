
const   util        = require('util');
var     request     = require('request');
var     async       = require('async');
var     phantom     = require('phantom');
const   fs          = require('fs');
var     scrape      = require('website-scraper');
var     download    = require('download-file')
var     progress    = require('request-progress');
var     ProgressBar = require('progress'); 
var     chalk       = require('chalk');
var     path        = require("path");

function bytesToSize(bytes,decimals) {
    if(bytes == 0) return '0 Bytes';
    var k = 1000,
        dm = decimals || 2,
        sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}


function convertTime(input, separator) {
    var pad = function(input) {return input < 10 ? "0" + input : input;};
    return [
        pad(Math.floor(input / 3600)),
        pad(Math.floor(input % 3600 / 60)),
        pad(Math.floor(input % 60)),
    ].join(typeof separator !== 'undefined' ?  separator : ':' );
}



module.exports = {

    videoDownload : async function(video){

        var promise =  new Promise(function(resolve, reject){ 

            var url = video.videoLink
            var bar;
            var fileUrl = url,
                title = video.name,
                apiPath = video.directory + '/' + video.name ,
                total_size = 0,
                total_downloaded = 0,
                remaining_time = 0;

            if (fs.existsSync(apiPath)){
                if(video.downloaded){
                    console.log('Video File exists : ' + video.name)
                    return resolve()
                }else{
                    fs.unlinkSync(apiPath);
                }
            }
            
                // The options argument is optional so you can omit it
                console.log(video.directory + '/' + video.name);
                let req = request(url)
                progress(req , {})
                .on('request',function(req){
                        bar = new ProgressBar(chalk.green("Downloading [:bar] :percent [:t_downloaded]  TOTAL SIZE :t_size / TIME REMAINING :download_time :complet"), {
                        complete: '>',
                        incomplete: ' ',
                        width: 50,
                        total: 100
                        });
                })
                .on('progress', function (state) {
                    total_size = bytesToSize(state.size.total);
                    remaining_time = convertTime(state.time.remaining);
                    total_downloaded = bytesToSize(state.size.transferred);

                    bar.update(state.percent,{
                        'complet' :  "",
                        't_size' : total_size,
                        'download_time' : remaining_time,
                        't_downloaded' : total_downloaded
                    });
                })
                .on('error', function (err) {
                    console.log(err);
                    console.log(chalk.red("Error Downloading please try again !!"));
                    return reject(err); 
                })
                .on('end', function () {
                        bar.update(1,{
                        'complet' :  "\u2713",
                            't_size' : total_size,
                            'download_time' : remaining_time,
                            't_downloaded' : total_size
                        });
                        console.log("\u2713");
                        console.log("\n");
                        return resolve()
                });
                req.pipe(fs.createWriteStream(apiPath)); 

            
        
        });
        return promise;
    },




    PdfDownload : async function(chapter , directory){
        
        var promise =  new Promise(function(resolve, reject){ 
            
            var url = chapter.slidesLink;
            if(url == null){
                return resolve() 
            }

            var options = {
                directory: directory,
                filename: 'CH_' +(chapter.number + 1) +'_slides.pdf'
            }
        

            if (fs.existsSync(directory + '/' + options.filename)){
                console.log('PDF File exists')
                return resolve()
            }

            try {
                download(url, options , function(){
                    console.log('Pdf downloaded for Course : ' + url)
                    return resolve()
                })
                
            } catch (error) {
                console.log(error.message)   
            }

        });

         return promise;

    }





}