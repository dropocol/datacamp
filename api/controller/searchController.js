
'use strict';

const   util        = require('util');
var     request     = require('request');
var     async       = require('async');
var     phantom     = require('phantom');
const   fs          = require('fs');
var     scrape      = require('website-scraper');
var     download    = require('download-file');

var     trackController = require('./trackController');
var     courseController = require('./courseController');
var     chapterController = require('./chapterController');
var     exerciseController = require('./exerciseController');

var searchController = function (){


    var getAllTracks =  async function(req , res , next){
        res.end();
        
        var skillTracks  = 'https://www.datacamp.com/tracks/skill'
        var careerTracks = 'https://www.datacamp.com/tracks/career'

        var skillsTrackLinks =  await trackController.getAllTrackLinksOnline(skillTracks);
        for (const [index, link] of skillsTrackLinks.entries()) {
            await _getTrackBasicInfo(link)
        }

        var careerTrackLinks =  await trackController.getAllTrackLinksOnline(careerTracks);
        for (const [index, link] of careerTrackLinks.entries()) {
            await _getTrackBasicInfo(link)
        }

    }




    var getTrackBasicInfo =  async function(req , res , next){

        var search = '';
        if(req.params.search_track == undefined){
            return res.json({
                message : 'Please provide a link'
            })
        }else{
            search = req.params.search_track
            res.end();
        }

        await _getTrackBasicInfo(search);

    }


    async function _getTrackBasicInfo(search){

        console.log('Getting Track courses .... wait');
        console.log('Track link : ' + search)

        const trackExist = await trackController.searchTrackInDbByLink(search);
        if(trackExist == undefined){

            const trackData = await trackController.searchTrackOnlineByLink(search)
            console.log(trackData.name);
            const track     = await trackController.saveTrackDataInDB(trackData)

            if(track.error){
                console.log(track.error.message)
                return
            }
           
            for (const [index, link] of track.courseLinks.entries()) {
                await getCourseDataFromDB(link , track , search)
            }

            await trackController.markTrackAsComplete(track._id);
            console.log('Track downloaded successfully :)')
        
        }else{

            for (const [index, link] of trackExist.courseLinks.entries()) {
                await getCourseDataFromDB(link , trackExist , search)
            }

            await trackController.deleteOldCourseIdsFromTrack(trackExist._id);
            await trackController.markTrackAsComplete(trackExist._id);
            console.log('Track downloaded successfully :)')

        }

        return

    }



    async function getCourseDataFromDB(link , track , search){

        console.log('\n\n\n')
        console.log('Getting course data for : ' + 'https://www.datacamp.com' + link);
        
        const courseExist = await courseController.searchCourseInDbByLink(link)
        if(courseExist == undefined){
            
            await getCourseData(link , track._id)
        
        }
        else if(courseExist !== undefined && courseExist.complete == true){
            
            let result = await trackController.saveCourseIdInTrack(courseExist._id , courseExist.link , track._id );
            if(result == true){
                await courseController.incrementCourseTrackCounter(courseExist._id)
            }

        }
        else if(courseExist !== undefined && courseExist.complete == false){
            
            let fullTrack = await trackController.getFullTrackData(search)

            var courseToDelete      = null;
            var chaptersToDelete    = []
            var exercisesToDelete   = []


            if(fullTrack.courses !== undefined && fullTrack.courses.length){
                fullTrack.courses.forEach(course => {
                    if(course._id.equals(courseExist._id) && course.complete == false){
                        courseToDelete = course
                    }
                });
            }

            

            if(courseToDelete != null){
                courseToDelete.chapters.forEach(chapter => {
                    chapter.exercises.forEach(exercise => {
                        exercisesToDelete.push(exercise._id);
                    });
    
                    chaptersToDelete.push(chapter._id);
                }); 

                await exerciseController.deleteExercisesFromDB(exercisesToDelete);
                await chapterController.deleteChaptersFromDB(chaptersToDelete);

                let deletedCourseId = await trackController.deleteCourseIdFromTrack(courseToDelete._id , track._id)                 
                if(deletedCourseId == true){
                    console.log('Getting course data for-4');
                    await courseController.deleteCourseFromDb(courseToDelete._id);
                }

            }else{


                var course = await courseController.getFullCourseData(courseExist._id);
                course.chapters.forEach(chapter => {
                    chapter.exercises.forEach(exercise => {
                        exercisesToDelete.push(exercise._id);
                    });
    
                    chaptersToDelete.push(chapter._id);
                }); 

                await exerciseController.deleteExercisesFromDB(exercisesToDelete);
                await chapterController.deleteChaptersFromDB(chaptersToDelete);
                await courseController.deleteCourseFromDb(course);
            }

            await getCourseData(link , track._id)
            return

        }
    }

  



    async function getCourseData(url , trackId){
      
        var courseData = null;

        if(trackId == null){
            const courseExist = await courseController.searchCourseInDbByLink(url);
            if(courseExist == undefined){
                courseData = await courseController.searchCourseOnlineByLink(url);
            }else{
                return;
            }
        }
        
        if(courseData == null){
            courseData = await courseController.searchCourseOnlineByLink(url);
        }
        
        console.log('Saving Course chapters and exercises in DB');
        var chapterIds          = await saveChaptersAndExercises(courseData);
        courseData.chapters     = chapterIds;
        
        var course              = await courseController.saveCourseDataInDB(courseData);
        if(course.error){
            console.log(course.error.message)
            return
        }

        if(trackId !== undefined && trackId != null){
            let result = await trackController.saveCourseIdInTrack(course._id, null , trackId);
            if(result == true){
                console.log('Increment Course Track Counter')
                await courseController.incrementCourseTrackCounter(course._id)
            }
        }

        console.log("Searching for the exercise links");
        await getExercisesDataFromDatabase(chapterIds)

        console.log('Save exercise addresses in DB')
        await exerciseController.saveExerciseAddressInDatabase(url)

        console.log("Mark course as complete : " + course._id);
        await courseController.markCourseAsComplete(course._id)

        return

    }






    async function saveChaptersAndExercises(chaptersData , link){

        var chapterIds = [];
        for (var [index, chapter] of chaptersData.chapters.entries()) {

            var chapterData   = chapter;
            var exerciseIds = []

            for (var [index, exerciseData] of chapterData.exercises.entries()) {
                exerciseData.course = chaptersData.link.substring(24 ,chaptersData.link.length) + '/' + (chapter.number + 1) + ' - ' + chapter.name
                let exercise        = await exerciseController.saveExerciseDataInDB(exerciseData)
                exerciseIds.push(exercise._id)
            }


            chapterData.exercises = exerciseIds
            let save_chapter = await chapterController.saveChapterDataInDB(chapterData)
            chapterIds.push(save_chapter._id);

        }

        return chapterIds;

    }





    async function getExercisesDataFromDatabase(chapterIds){
        
        for (const [index, chapterId] of chapterIds.entries()) {
            var chapter = await models.chapter.findById(chapterId)
            .populate({
                path    : 'exercises'
            })
            .exec()
            await getExercisesVideoLinks(chapter)
        }

        return

    }



    async function getExercisesVideoLinks(chapter){


        var scriptData = null;

        for (const [index, exercise] of chapter.exercises.entries()) {

            if(exercise.type == 'video'){

                console.log('\n\nGetting the iFrame');
                
                var exerciseData = {};

                if(scriptData == null){

                    exerciseData        = await exerciseController.searchExerciseOnlineByLink(exercise.link)
                    scriptData          = exerciseData.script;
                    
                    if(scriptData.iframeUrl == null){
                        exerciseData = getNewVideoDownloadLink(scriptData , index)
                    }
                
                }else{
                    exerciseData = getNewVideoDownloadLink(scriptData , index)
                }


                console.log('Getting video download link : ' +  exerciseData.iframeUrl);
                var videoData           = await exerciseController.extractDownloadLinkForVideo(exerciseData.iframeUrl)
                //videoData.iframeUrl     = exerciseData.iframeUrl;
                console.log(videoData)
                
                await exerciseController.updateExerciseVideoLink(exercise._id , videoData)
                                    
                if(index == 0){
                    await chapterController.updateChapterPdfLink(exerciseData.script)
                }

            }
        }

        return

    }




    
    function getNewVideoDownloadLink(scriptData , index){
        
        var data                = {};
        var exercises           = scriptData.exercises.all
        var currentExercise     = exercises[index];
        var currentExerciseKey  = exercises[index].projector_key;

        if(currentExerciseKey != null){
        
            data.iframeUrl  = 'https://projector.datacamp.com/?auto_play=play&playback_rate=1&quality=auto&projector_key=' + currentExerciseKey;
            data.script     = scriptData; 
        
        }else{

            console.log('Generating new link');
            var hls                 = currentExercise.video_hls;
            data.iframeUrl          = 'https://projector.datacamp.com/?auto_play=play&playback_rate=1&quality=auto&video_hls='  + hls
            data.script             = scriptData;
        }

        return data;

    }










    var getExercisesLinksUsingChapterId =  function(req , res , next){

        console.log('Finding Chapter')
        models.chapter.findById(req.params.chapter_id)
        .populate({
            path    : 'exercises'
        })
        .exec()
        .then((chapter) => {
            res.status(200).json({ 
                success:true,
                chapter : chapter
            });
            //getExercisesLinks(chapter)
        })
        .catch((err) => {
            return 'error occured';
        });
    }






    var removeDatabase =  function(req , res , next){

        models.track.remove({} , function(){
            console.log('Tracks Removed')
        })
        models.course.remove({} , function(){
            console.log('Course Removed')
        })
        models.chapter.remove({} , function(){
            console.log('Chapter Removed')
        })
        models.exercise.remove({} , function(){
            console.log('Exercise Removed')
        })
        models.exerciseAddress.remove({} , function(){
            console.log('Exercise Removed')
        })

        res.end();
    }



    var getAllCourses = async function(req , res){
        
        console.log('Getting Links for all courses');
        res.end();
        var courseLink = await courseController.getAllCoureLinksOnline();
        
        
        for (const link of courseLink) {
            await getCourseData(link , null);
        }

    }


    var deleteCourse = async function(req, res){
        var search = req.params.search_course;
        res.end();
        await courseController.deleteCourseFromDbByLink(search);
    }


    var getCourse = async function(req, res){
        var search = '/courses/' + req.params.search_course;
        await getCourseData(search , null)
    }


    var deleteTrack =  async function(req , res , next){
        
        var search = '';
        if(req.params.search_track == undefined){
            return res.json({
                message : 'Please provide a link'
            })
        }else{
            search = req.params.search_track
            res.end();
        }

        await trackController.deleteTrackFromDB(search)

    }




  
    return {
        getTrackBasicInfo                       :       getTrackBasicInfo,
        getCourseData                           :       getCourseData,
        getExercisesLinksUsingChapterId         :       getExercisesLinksUsingChapterId,
        removeDatabase                          :       removeDatabase,
        deleteTrack                             :       deleteTrack,
        getAllCourses                           :       getAllCourses,
        deleteCourse                            :       deleteCourse,
        getCourse                               :       getCourse,
        getAllTracks                            :       getAllTracks
        
    };

};


module.exports = searchController;






