var     phantom     = require('phantom');


function replaceIllegalCharacters(str) {
    var string = str
    string =  string.replace(new RegExp('/ ', 'g'), '-');
    string =  string.replace(new RegExp('/', 'g'), '-');
    string =  string.replace(new RegExp('&amp;', 'g'), 'and');
    string =  string.replace(new RegExp(': ', 'g'), '-');
    string =  string.replace(new RegExp(':', 'g'), '-');
    return string;
}



module.exports = {


    // searchCourseInDbByLink : async function(link){
    //     try {
    //         var query = { link : 'https://www.datacamp.com' + link}
    //         const result = await models.track.findOne(query).exec()
    //         return result

    //     } catch (error) {
    //         return ({error : error})
    //     }
    // },


    updateChapterPdfLink : async function(script){

        var courseUrl   = 'https://www.datacamp.com/courses/' + script.course.slug
        var query       = {link : courseUrl};
        var course      = await models.course.findOne(query).exec()
        var chapterIds  = course.chapters;
        for (const [index, id] of chapterIds.entries()) {
            var slidesLink      =  script.course.chapters[index].slides_link
            var chapter         = await models.chapter.findById(id).exec()
            chapter.slidesLink  = slidesLink
            chapter.complete    = true
            await chapter.save()
        }

        return

    },




    saveChapterDataInDB : async function(chapterData){

        var chapter       = await new models.chapter(chapterData)
        chapter.name      = replaceIllegalCharacters(chapter.name)
        try {
            await chapter.save();
            return chapter;
        } catch (error) {
            return ({error : error})
        }        
    },



    deleteChaptersFromDB : async function(chaptersToDelete){
        for (const [index, id] of chaptersToDelete.entries()) {
            try {
                await models.chapter.findByIdAndRemove(id).exec()
            } catch (error) {
                console.log(error.message);
                //return ({error : error})
            }  
        }

        return
    }

}