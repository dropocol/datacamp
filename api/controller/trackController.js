var     phantom                     = require('phantom');
const   cheerio                     = require('cheerio')
var     rp                          = require('request-promise');

var     courseController        = require('./courseController');
var     chapterController       = require('./chapterController');
var     exerciseController      = require('./exerciseController');
var     fileController          = require('./fileController');
var     downloadFile            = require('./download');


function replaceIllegalCharacters(str) {
    var string = str
    string =  string.replace(new RegExp('/ ', 'g'), '-');
    string =  string.replace(new RegExp('/', 'g'), '-');
    string =  string.replace(new RegExp('&amp;', 'g'), 'and');
    string =  string.replace(new RegExp(': ', 'g'), '-');
    string =  string.replace(new RegExp(':', 'g'), '-');
    string =  string.replace(new RegExp('  ', 'g'), '');
    string =  string.replace(new RegExp('\n', 'g'), '');
    string =  string.replace(new RegExp('with', 'g'), ' with');
    return string;
}


module.exports = {

    searchTrackInDbByLink : async function(link){
        try {   
            var query = { link : 'https://www.datacamp.com/tracks/' + link}
            var result = await models.track.findOne(query).exec()
            return result
        } catch (error) {
            ({error : error})
        }
    },

    getAllTrackLinksInDB : async function(url){
        try {   

            var data  = [];
            var query = {};
            if(url == '/skill_tracks'){
                query.type = 'skill'
            }else{
                query.type = 'career'
            }

            var result = await models.track.find(query).exec()

            for (const [index, track] of result.entries()) { 
                var trackData   = {}
                trackData.name  = track.name;
                trackData.link  = track.link.substring(24, track.link.length);
                data.push(trackData);
            }

            return data

        } catch (error) {
            ({error : error})
        }
    },


    searchTrackOnlineByLink : async function(link){

        const instance  = await phantom.create();
        const page      = await instance.createPage();
        
        const url       =  'https://www.datacamp.com/tracks/' + link
        const status    = await page.open(url);
        const content   = await page.property('content');
        
        await page.on('onConsoleMessage' , function(data) {
            console.log(data);
        });

        console.log('Analyzing Track page : ' + url);
        let data = await page.evaluate(function() {

            console.log('Getting data from Track page');
            var title  = document.querySelectorAll('.header-hero__title')[0].innerHTML;

            const url       = document.URL;
            // console.log(JSON.stringify(document.querySelectorAll('.header-hero__description > p')))
            const description = document.querySelectorAll('.header-hero__description > p')[0].innerHTML;
            const type        = document.querySelectorAll('.header-hero__uptitle')[0].innerHTML;
            
            var links = [];
     
            const totalLinks = document.querySelectorAll('a.course-block__link').length;
            for (i = 0; i < totalLinks; i++){
                const link = document.querySelectorAll('a.course-block__link')[i].getAttribute('href')
                links.push(link)
            }

            var data = {};
            data.link              = url
            data.name              = title
            data.description       = description
            data.courseLinks       = links
            data.type              = type
            return data 
        })

        data.name = replaceIllegalCharacters(data.name);
        await instance.exit();
        return data
    },




    saveTrackDataInDB : async function(trackData){
        let track       = await new models.track(trackData)

        if(trackData.type == 'SKILL TRACK'){
            track.type = 'skill'
        }else if(trackData.type == 'CAREER TRACK'){
            track.type = 'career'
        }

        try {
            console.log('Saving Track page in DB');
            await track.save();
            return track;
        } catch (error) {
            return ({error : error})
        }        
    },


    saveCourseIdInTrack : async function(courseId , courseLink , trackId){
        try {   

            var track = await models.track.findById(trackId).exec()
            if(track.courses.indexOf(courseId) == -1){

                if (courseLink != null){
                    var link     = courseLink.substring(24, courseLink.length);
                    var courseIndex = track.courseLinks.indexOf(link)
                    track.courses.splice(courseIndex , 0 , courseId)
                    
                    //console.log('Save - 1 : ' + courseIndex)
                    await track.save();
                    return true
                }else{
                    //console.log('Save - 2')
                    track.courses.push(courseId)
                    await track.save();
                    return true
                }

            }else{
                return false
            }
            
        } catch (error) {
            return ({error : error})
        }
    },





    getFullTrackData : async function(link){

        query = {link : 'https://www.datacamp.com/tracks/' + link }
        try {
            var track = await models.track.findOne(query)
            .populate({
                path    : 'courses',
                populate: [
                    { path:  'chapters' , model: 'Chapter' , populate: [{ path:  'exercises' , model: 'Exercise'}]}
                ]
            })
            .exec()
            return track;

        } catch (error) {
            ({error : error})
        }


    },




    deleteCourseIdFromTrack : async function(courseId , trackId){
        
        var track = await models.track.findById(trackId).exec()
        var index = track.courses.indexOf(courseId);
        if(index != -1){
            track.courses.splice(index, 1);
            try {
                await track.save()
                return true
            } catch (error) {
                console.log(error.message);
                //return ({error : error})
                return false 
            }  
        }else{
            return false
        }
    },



    deleteOldCourseIdsFromTrack : async function(trackId){

        var track = await models.track.findById(trackId).exec()
        var confirmedIds = [];
        for (const [index, id] of track.courses.entries()) {
            var course = await models.course.findById(id).exec()
            if(course != undefined){
                confirmedIds.push(id)
            }
        }
        
        try {
            track.courses = confirmedIds;
            await track.save()
            return
        } catch (error) {
            console.log(error.message);
            //return ({error : error})
            return
        }  
        
    },



    getTrackCoursesInDB : async function(link){
        try {   
            var data  = {};
            //var query = {complete : true};
            var query = {link : 'https://www.datacamp.com/tracks/' + link }
            var track = await models.track.findOne(query)
            .populate({
                path    : 'courses'
            })
            .exec()

            //console.log(track.courses);

            data.trackName = track.name;
            data.courses   = [];
            for (const [index, course] of track.courses.entries()) { 
                var courseData   = {}
                courseData.name  = course.name;
                courseData.link  = course.link.substring(24, course.link.length);
                data.courses.push(courseData);
            }

            // console.log(data)
            return data

        } catch (error) {
            ({error : error})
        }
    },





    markTrackAsComplete : async function(trackId){
        try {
            const track                = await models.track.findById(trackId).exec()
            track.complete             = true;
            await track.save();
            return

        } catch (error) {
            return ({error : error})
        }      
    },




    deleteTrackFromDB : async function(link){

        var fullTrack = await this.getFullTrackData(link);

        var coursesToDelete     = [];
        var chaptersToDelete    = [];
        var exercisesToDelete   = [];
        

        fullTrack.courses.forEach(course => {

            coursesToDelete.push(course._id);
            
            if(course.connectedToTracks == 1){
                course.chapters.forEach(chapter => {
                    chapter.exercises.forEach(exercise => {
                        exercisesToDelete.push(exercise._id);
                    });
                    chaptersToDelete.push(chapter._id);
                }); 
            }

        }); 

        console.log('Courses to Delete  : ' + coursesToDelete.length)
        console.log('Chapters to Delete : ' + chaptersToDelete.length)
        console.log('Exercise to Delete : ' + exercisesToDelete.length)


        try {
            await exerciseController.deleteExercisesFromDB(exercisesToDelete);
            await chapterController.deleteChaptersFromDB(chaptersToDelete);

            for (const id of coursesToDelete) {
                await courseController.deleteCourseFromDb(id);
            }

            await fullTrack.remove();  
            console.log('Track removed');
        } catch (error) {
            console.log('An error occured');
            console.log(error)
        }
    },






    getAllTrackLinksOnline : async function(link){

        var promise =  new Promise(function(resolve, reject){ 
            rp(link)
            .then(function (htmlString) {

                var $ = cheerio.load(htmlString);
                var tracks = [];

                $('.shim').each(function(i, elem) {
                    let link = $(this).attr('href');
                    link     = link.substring(8 , link.length);
                    console.log(link);
                    if(tracks.indexOf(link) == -1 && link != '/' && link != ''){
                        tracks.push(link);
                    }
                });

                return resolve(tracks)

            })
            .catch(function (err) {
                console.log(err)
            });
        
        });

        return promise;
    }











}

