'use strict';

// load the things we need
var mongoose  = require('mongoose');
const options = { timestamps: true ,  usePushEach: true }

// define the schema for our user model
var exerciseAddressSchema = mongoose.Schema({
        address            :  {type:String   , required: true },
        name               :  {type:String   , required: true },
        link               :  {type:String   , index:{unique: true} , required: true },
        type               :  {type:String   , enum: ['video' , 'webpage' , 'projector' , 'audio']},
        projectorData   : {
                name                    : {type:String},
                audioLink               : {type:String},
                videoLink               : {type:String},
                type                    : {type:String  , enum: ['audio' , 'video'] }
        },
        videoData   : {
                name                    : {type:String},
                videoLink               : {type:String}
        },  
} , options);

// create the model for users and expose it to our app
module.exports = mongoose.model('ExerciseAddress', exerciseAddressSchema);