'use strict';

// load the things we need
var mongoose = require('mongoose');
const options = { timestamps: true ,  usePushEach: true }

// define the schema for our user model
var courseSchema = mongoose.Schema({
        courseId                :  {type:String},
        link                    :  {type:String  , index: {unique: true} , required: true },
        name                    :  {type:String  , required: true },
        description             :  {type:String},
        chapters                :  [{type:mongoose.Schema.Types.ObjectId  , ref: 'Chapter'}],
        connectedToTracks       :  {type:Number  , default : 0 },
        complete                :  {type:Boolean  , default: false }
} , options);



// create the model for users and expose it to our app
module.exports = mongoose.model('Course', courseSchema);
