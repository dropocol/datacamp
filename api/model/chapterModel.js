'use strict';

// load the things we need
var mongoose = require('mongoose');
const options = { timestamps: true ,  usePushEach: true }

// define the schema for our user model
var chapterSchema = mongoose.Schema({
        name            :  {type:String  , required: true },
        link            :  {type:String  , required: true },
        slidesLink      :  {type:String},
        number          :  {type:Number  , required: true },
        description     :  {type:String  , required: true },
        exercises       :  [{type:mongoose.Schema.Types.ObjectId  , ref: 'Exercise'}],
        complete        :  {type:Boolean  , default: false }
} , options);



// create the model for users and expose it to our app
module.exports = mongoose.model('Chapter', chapterSchema);
