'use strict';

// load the things we need
var mongoose = require('mongoose');
const options = { timestamps: true ,  usePushEach: true }

// define the schema for our user model
var trackSchema = mongoose.Schema({

        link         :  {type:String  , index: {unique: true} , required: true },
        name         :  {type:String  , required: true },
        description  :  {type:String},
        courseLinks  :  [{type:String  , required: true }],
        courses      :  [{type:mongoose.Schema.Types.ObjectId  , ref: 'Course'}],
        complete     :  {type:Boolean  , default: false },
        type         :  {type:String   , enum: ['skill' , 'career']}
        
} , options);



// create the model for users and expose it to our app
module.exports = mongoose.model('Track', trackSchema);
