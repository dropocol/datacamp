'use strict';

// load the things we need
var mongoose = require('mongoose');
const options = { timestamps: true ,  usePushEach: true }

// define the schema for our user model
var exerciseSchema = mongoose.Schema({
        number          :  {type:Number  , required: true },
        name            :  {type:String  , required: true },
        course          :  {type:String  , required: true },
        link            :  {type:String  , index: {unique: true} ,  required: true },
        downloaded      :  {type : Boolean , default : true},
        type            :  {type:String  , enum: ['video' , 'webpage' , 'projector' , 'audio'] , required: true },
        projectorData   : {
                videoLink               : {type:String},
                audioLink               : {type:String},
                projectorLink           : {type:String},
                type                    : {type:String  , enum: ['audio' , 'video'] }
        },
        videoData   : {
                videoLink          : {type:String},
                projectorLink      : {type:String}
        },
        complete        :  {type:Boolean  , default: false }
} , options);

// create the model for users and expose it to our app
module.exports = mongoose.model('Exercise', exerciseSchema);
