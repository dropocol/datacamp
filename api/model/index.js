module.exports = {
    track               : require('./trackModel.js'),
    course              : require('./courseModel.js'),
    chapter             : require('./chapterModel.js'),
    exercise            : require('./exerciseModel.js'),
    exerciseAddress     : require('./exerciseAddressModel.js'),
}